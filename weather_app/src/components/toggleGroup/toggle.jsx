import * as React from "react";
import { useState } from "react";
import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import SsidChartIcon from "@mui/icons-material/SsidChart";
import GridViewIcon from "@mui/icons-material/GridView";
import TableComponent from "../table/table";

export default function ToggleComponent(props) {
  const { info, degree, tId } = props;
  const [alignment, setAlignment] = useState("grid");
  const [toggleDay, setToggleDay] = useState(tId);
  const [day, setDay] = useState(tId);

  const toggleBtnHandleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };
  const dayToggleBtnHandleChange = (event, newAlignment) => {
    setToggleDay(parseInt(newAlignment));
  };
  const dayHandleChange = (e) => {
    setDay(e.target.value);
  };
  console.log("data", day);
  return (
    <>
      <div className="toggle">
        <ToggleButtonGroup
          color="primary"
          exclusive
          size="small"
          value={toggleDay}
          onChange={dayToggleBtnHandleChange}
        >
          <ToggleButton value={0} onClick={dayHandleChange}>
            Sunday
          </ToggleButton>
          <ToggleButton value={1} onClick={dayHandleChange}>
            monday
          </ToggleButton>
          <ToggleButton value={2} onClick={dayHandleChange}>
            Tuesday
          </ToggleButton>
          <ToggleButton value={3} onClick={dayHandleChange}>
            Wednesday
          </ToggleButton>
          <ToggleButton value={4} onClick={dayHandleChange}>
            Thursday
          </ToggleButton>
          <ToggleButton value={5} onClick={dayHandleChange}>
            Friday
          </ToggleButton>
          <ToggleButton value={6} onClick={dayHandleChange}>
            Saturday
          </ToggleButton>
        </ToggleButtonGroup>
        <ToggleButtonGroup
          color="primary"
          exclusive
          value={alignment}
          onChange={toggleBtnHandleChange}
        >
          <ToggleButton value="grid">
            <GridViewIcon />
          </ToggleButton>
          <ToggleButton value="ssid">
            <SsidChartIcon />
          </ToggleButton>
        </ToggleButtonGroup>
      </div>

      <TableComponent info={info} day={day} degree={degree} />
    </>
  );
}
