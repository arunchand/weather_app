import * as React from "react";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";

export default function TableComponent(props) {
  const { info, degree, day } = props;

  return (
    <TableContainer component={Paper} className="table-div">
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <strong>Sunday</strong>
            </TableCell>
            <TableCell>
              <strong>Icon</strong>
            </TableCell>
            <TableCell>
              <strong>Temp</strong>
            </TableCell>
            <TableCell>
              <strong>Wind</strong>
            </TableCell>
            <TableCell>
              <strong>Precip.</strong>
            </TableCell>
            <TableCell>
              <strong>Cloud</strong>
            </TableCell>
            <TableCell>
              <strong>Humidity</strong>
            </TableCell>
            <TableCell>
              <strong>Pressure</strong>
            </TableCell>
          </TableRow>
        </TableHead>
        {info?.forecast?.forecastday[day]?.hour
          .filter((item) => parseInt(item.time.slice(11, 13)) % 4 === 0)
          .map((each) => (
            <TableBody>
              <TableCell>{each.time.slice(11)}</TableCell>
              <TableCell>
                <img src={each.condition.icon} className="image"></img>
              </TableCell>
              {degree === "F" ? (
                <TableCell>{each.temp_f} °F</TableCell>
              ) : (
                <TableCell>{each.temp_c} °C</TableCell>
              )}

              <TableCell>{each.wind_kph} kmph</TableCell>
              <TableCell>{each.precip_mm} mm</TableCell>
              <TableCell>{each.cloud}%</TableCell>
              <TableCell>{each.humidity}%</TableCell>
              <TableCell>{each.pressure_mb} mb</TableCell>
            </TableBody>
          ))}
      </Table>
    </TableContainer>
  );
}
