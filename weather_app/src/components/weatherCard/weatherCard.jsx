import * as React from "react";
import './weatherCard.css';

export default function WeatherCard(props) {
  const { info, degree, today } = props;
  return (
    <>
      <div className="container-div">
        <div className="inner-div">
          <div className="sunday">
            <img
              src={info?.forecast?.forecastday[0]?.day.condition.icon}
              className="image"
              style={{ height: "90px" }}
            ></img>
            <div className="sunday-info">
              {degree === "F" ? (
                <p className="card-heading">
                  {info?.forecast?.forecastday[0]?.day.maxtemp_f} °f {today}{" "}
                  <br />
                  <p className="condition-text">
                    {info?.forecast?.forecastday[0]?.day.condition.text}
                  </p>
                </p>
              ) : (
                <p className="card-heading">
                  {info?.forecast?.forecastday[0]?.day.maxtemp_c} °C {today}{" "}
                  <br />
                  <p className="condition-text">
                    {info?.forecast?.forecastday[0]?.day.condition.text}
                  </p>
                </p>
              )}
            </div>
          </div>
          <div className="heading_info">
            {console.log("sundayyyy", info?.current?.wind_mph)}
            <p>
              Wind <br />
              {info?.current?.wind_mph} mph
            </p>
            <p>
              Precip. <br />
              {info?.current?.precip_mm} mm
            </p>
            <p>
              Pressure <br />
              {info?.current?.pressure_in} in
            </p>
          </div>
        </div>
        <div className="weeks">
          {info?.forecast?.forecastday?.map((each) => (
            <div>
              <img src={each.day.condition.icon} className="image" />
              {degree === "F" ? (
                <p>{each.day.avgtemp_f} °F</p>
              ) : (
                <p>{each.day.avgtemp_c} °C</p>
              )}

              <p>{each.day.condition.text}</p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
