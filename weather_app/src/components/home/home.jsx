import * as React from "react";
import { useState } from "react";
// Material UI imports
import { ToggleButtonGroup, ToggleButton } from "@mui/material";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
// Components Imports
import ToggleComponent from "../toggleGroup/toggle";
import WeatherCard from "../weatherCard/weatherCard";
import WeatherToday from "../weatherToday/weatherToday";
import "./home.css";

export default function Home() {
  const [info, setInfo] = useState("");
  const [searchValue, setsearchValue] = useState("");
  const [degree, setDegree] = React.useState("C");
  const [loading, setLoading] = useState(false);
  const toggleTempHandleChange = (event, newAlignment) => {
    setDegree(newAlignment);
  };
  const changeHandler = (e) => {
    setsearchValue(e.target.value);
  };
  // Submit Handler for Search Value.
  const submitHandler = (e) => {
    e.preventDefault();
    fetchData();
  };
  // Fetching data from Api
  const fetchData = () => {
    const url = `https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=${searchValue}&days=7&aqi=no&alerts=no`;

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setInfo(data);
        setLoading(true);
        // console.log("data", data.error.message)
        if(data?.error?.message){
          setLoading(false)
        }
      })
  };
  const weekday = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  let t = new Date(),
    tId = t.getDay(),
    today = weekday[tId];
    console.log("day",today,tId);

  return (
    <div>
      {console.log("info", info)}
      {/* {console.log("date", info?.forecast?.forecastday[0]?.date)} */}
      <div className="main">
        {/* Search field */}
        <form onSubmit={submitHandler}>
          <TextField
            className="searchbar"
            type="search"
            placeholder="Please Enter City Name, US Zip Code, Canada Postal Code, UK PostCode, ip, metar, etc."
            onChange={changeHandler}
          ></TextField>
        </form>
        {loading === false ? (
          <h3>
            <br />
            <br />
            {info?.error?.message} <br />
            Please Enter a Valid City Name.
          </h3>
        ) : (
          <div>
            {/* Main Heading */}
            <div className="heading-div">
              <h2 className="top-heading">
                {info.location?.name} Weather Forecast{" "}
                <span>
                  {info.location?.region}, {info.location?.country}
                </span>
              </h2>
              <ToggleButtonGroup
                color="primary"
                exclusive
                value={degree}
                onChange={toggleTempHandleChange}
              >
                <ToggleButton value="F">°F</ToggleButton>
                <ToggleButton value="C">°C</ToggleButton>
              </ToggleButtonGroup>
            </div>
            <WeatherCard info={info} degree={degree} today={today}/>
            <WeatherToday info={info} degree={degree} />
            <ToggleComponent info={info} degree={degree} tId={tId}/>
          </div>
        )}
      </div>
    </div>
  );
}
