import * as React from "react";

export default function WeatherToday(props) {
  const { info, degree } = props;
  return (
    <>
      <h2 className="top-heading">Today's Weather In {info.location?.name}</h2>
      <div className="today-weather">
        <div>
          <p> Sunrise {info?.forecast?.forecastday[0]?.astro.sunrise}</p>
          <p> Sunset {info?.forecast?.forecastday[0]?.astro.sunset}</p>
        </div>
        <div>
          <p> Moonrise {info?.forecast?.forecastday[0]?.astro.moonrise}</p>

          <p> Moonrise {info?.forecast?.forecastday[0]?.astro.moonset}</p>
        </div>

        <div className="all-container">
          {degree === "F" ? (
            <p>
              Max. <br />
              {info?.forecast?.forecastday[0]?.day.maxtemp_f} °F
            </p>
          ) : (
            <p>
              Max. <br />
              {info?.forecast?.forecastday[0]?.day.maxtemp_c} °C
            </p>
          )}
          {degree === "F" ? (
            <p>
              Max. <br />
              {info?.forecast?.forecastday[0]?.day.mintemp_f} °F
            </p>
          ) : (
            <p>
              Max. <br />
              {info?.forecast?.forecastday[0]?.day.mintemp_c} °C
            </p>
          )}

          <p>
            Avg. <br />
            {info?.forecast?.forecastday[0]?.astro?.sunrise}
          </p>
          <p>
            Precip. <br />
            {info?.forecast?.forecastday[0]?.day.totalprecip_mm}mm
          </p>

          <p>
            Max Wind <br />
            {info?.forecast?.forecastday[0]?.day.avgvis_km}Kph
          </p>
        </div>
      </div>
    </>
  );
}
